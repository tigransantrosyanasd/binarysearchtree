#pragma once

template <typename T1>
struct Node {
    T1 m_info;
    Node<T1>* m_left;
    Node<T1>* m_right;

    explicit Node(const T1& info)
            : m_info(info)
            , m_left(nullptr)
            , m_right(nullptr) {}

    bool is_leaf() const {
        return (m_left == nullptr && m_right == nullptr);
    }

    bool has_left() const {
        return (m_left != nullptr);
    }

    bool has_right() const {
        return (m_right != nullptr);
    }

    bool has_both() const {
        return (m_left != nullptr && m_right != nullptr);
    }
};

template <typename T>
class BinaryTree {

public:
     BinaryTree() : m_root(nullptr){}
    explicit BinaryTree(T info) {
        m_root = new Node<T>(info);
    }
    virtual ~BinaryTree() {
        destroy();
    }

protected:
    Node<T>*  m_root;

public:
    void recursive_preorder_traversal() {
        recursive_preorder_traversal(m_root);
    }
    void recursive_inorder_traversal() {
        recursive_inorder_traversal(m_root);
    }
    void recursive_postorder_traversal() {
        recursive_postorder_traversal(m_root);
    }
    void iterative_preorder_traversal() {
        iterative_preorder_traversal(m_root);
    }
    void iterative_inorder_traversal() {
        iterative_inorder_traversal(m_root);
    }
    void iterative_postorder_traversal() {
        iterative_postorder_traversal(m_root);
    }
    void iterative_levelorder_traversal() {
        iterative_levelorder_traversal(m_root);
    }

private:
    void recursive_preorder_traversal(Node<T>* root);
    void recursive_inorder_traversal(Node<T>* root);
    void recursive_postorder_traversal(Node<T>* root);
    void iterative_preorder_traversal(Node<T>* root);
    void iterative_inorder_traversal(Node<T>* root);
    void iterative_postorder_traversal(Node<T>* root);
    void iterative_levelorder_traversal(Node<T>* root);


private:
    void destroy(Node<T>* root);
    int height(Node<T>* root);
    int count_of_nodes(Node<T>* root);
    int count_of_elements_until_level_n(Node<T>* root, int n);

public:
    void destroy();
    int height();
    int count_of_nodes();
};

class BinarySearchTree:public BinaryTree<int> {
public:
    explicit BinarySearchTree(int = -1);
    ~BinarySearchTree() override {destroy();}

public:
    void insert(int);
};