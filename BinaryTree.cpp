#include <iostream>
#include "BinaryTree.h"
#include <stack>
#include <queue>
template <typename T>
void BinaryTree<T>::recursive_inorder_traversal(Node<T>* root) {
    if(root == nullptr) return;
    recursive_inorder_traversal(root->m_left);
    std::cout << root->m_info;
    recursive_inorder_traversal(root->m_right);
}

template <typename T>
void BinaryTree<T>::recursive_preorder_traversal(Node<T>* root) {
    if(root == nullptr) return;
    std::cout << root->m_info;
    recursive_preorder_traversal(root->m_left);
    recursive_preorder_traversal(root->m_right);
}

template <typename T>
void BinaryTree<T>::recursive_postorder_traversal(Node<T>* root) {
    if(root == nullptr) return;
    recursive_postorder_traversal(root->m_left);
    recursive_postorder_traversal(root->m_right);
    std::cout << root->m_info;
}
template <typename T>
void BinaryTree<T>::iterative_inorder_traversal(Node<T>* root) {
    std::stack<Node<T>*> a;
    Node<T>* current = root;
    while( !a.empty() || current != nullptr  ) {
        while (current != nullptr) {
            a.push(current);
            current = current->m_left;
        }
        current = a.top();
        std::cout << current->m_info;
        a.pop();
        current = current->m_right;
    }
}

template <typename T>
void BinaryTree<T>::iterative_preorder_traversal(Node<T>* root) {
    std::stack<Node<T>*> a;
    Node<T>* current = root;
    while( !a.empty() || current != nullptr  ) {
        while (current != nullptr) {
            a.push(current);
            std::cout << current->m_info;
            current = current->m_left;
        }
        current = a.top()->m_right;
        a.pop();
    }
}

template <typename T>
void BinaryTree<T>::iterative_postorder_traversal(Node<T>* root) {
        Node<T>* current = m_root;
        std::stack<Node<T>*> a;
        while (!a.empty() || current != nullptr) {
            while (current != nullptr ) {
                a.push(current);
                a.push(current);
                current = current->m_left;
            }
            current = a.top();
            a.pop();
            if (!a.empty() && a.top() == current) {
                current = current->m_right;
            }
            else {
                std::cout  << current->m_info;
                current = nullptr;
            }
        }
}

template <typename T>
void BinaryTree<T>::iterative_levelorder_traversal(Node<T>* root) {
    std::queue<Node<T>*> a;
    a.push(m_root);
    while(!a.empty()) {
        if(a.front()->m_left != nullptr) {
            a.push().a.front()->m_left;
        }
        if(a.front()->m_right != nullptr) {
            a.push().a.front()->m_right;
        }
        std::cout << a.front();
        a.pop();
    }
}

template <typename T>
void BinaryTree<T>::destroy(Node<T>* root) {
    if(root == nullptr) return;
    recursive_postorder_traversal(root->m_left);
    recursive_postorder_traversal(root->m_right);
    delete root;
}

template <typename T>
int BinaryTree<T>::height(Node<T>* root) {
    if(root == nullptr) {return 0;}
    return 1+ std::max(height(root->m_left),height(root->m_right));
}

template <typename T>
int BinaryTree<T>::count_of_nodes(Node<T>* root) {
    if(root == nullptr) {return 0;}
    return 1+ count_of_nodes(root->m_left) + count_of_nodes(root->m_right);
}

template <typename T>
void BinaryTree<T>::destroy() {
    destroy(m_root);
}

template <typename T>
int BinaryTree<T>::height() {
    height(m_root);
}

template <typename T>
int BinaryTree<T>::count_of_nodes() {
    count_of_nodes(m_root);
}

template <typename T>
int BinaryTree<T>::count_of_elements_until_level_n(Node<T>* root, int n) {

}



BinarySearchTree::BinarySearchTree(int info): BinaryTree(info) {}

void BinarySearchTree::insert(int info) {
    Node<int>* current = m_root;
    while(current != nullptr ) {
        if(info < current->m_info) {
            if(current->has_left()) {
                current = current->m_left;
            }
            else {
                current->m_left = new Node<int>(info);
                break;}
        }
        if(info >= current->m_info) {
            if (current->has_right()) {
                current = current->m_right;
            }
            else {
                current->m_right  = new Node<int>(info);
                break;}
        }

    }
    current = new Node<int>(info);
}



